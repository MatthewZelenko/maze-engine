#include "Sandbox.h"
#include "Layer/SandboxLayer.h"


Sandbox::Sandbox() : Maze::Application("Maze Engine: Sandbox")
{

}

Sandbox::~Sandbox()
{
}

bool Sandbox::Init()
{
	GetLayerStack().CreateLayer<SandboxLayer>();

	return true;
}

void Sandbox::Update()
{
}
void Sandbox::Render()
{
}

void Sandbox::Destroy()
{

}
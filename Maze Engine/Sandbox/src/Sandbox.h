#pragma once
#include "Maze.h"

class Sandbox : public Maze::Application
{
public:
	Sandbox();
	~Sandbox();



	// Inherited via Application
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Render() override;
	virtual void Destroy() override;

private:
};
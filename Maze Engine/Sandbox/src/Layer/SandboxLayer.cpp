#include "SandboxLayer.h"
#include <Core/Log.h>
#include "Core/Time.h"
#include <Core/LayerStack.h>
#include <Events/KeyEvent.h>
#include <imgui/imgui.h>

SandboxLayer::SandboxLayer()
{
}

SandboxLayer::~SandboxLayer()
{
}

void SandboxLayer::Init()
{
	GetLayerStack().SubscribeToEvent<Maze::KeyPressedEvent>(this, std::bind(&SandboxLayer::OnKeyPressed, this, std::placeholders::_1));
}
void SandboxLayer::Update()
{


	// LOG_CLIENT_TRACE("Update: {0}:{1}", TIME_FPS(), TIME_RUNTIME_IN_MINUTES());
}

void SandboxLayer::Render()
{
}

void SandboxLayer::UIRender()
{
}


bool SandboxLayer::OnKeyPressed(Maze::KeyPressedEvent* a_event)
{
	// LOG_CLIENT_TRACE("Captured key pressed event: {0}", a_event->GetKey());
	return true;
}
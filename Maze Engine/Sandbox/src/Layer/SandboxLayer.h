#pragma once
#include <stdint.h>
#include <Core/Layer.h>
#include <Events/ApplicationEvent.h>
#include <Events/KeyEvent.h>
#include <Events/MouseEvent.h>

class SandboxLayer : public Maze::Layer
{
public:
	SandboxLayer();
	~SandboxLayer();

	void Init();
	void Update() override;
	void Render() override;
	void UIRender() override;

	bool OnKeyPressed(Maze::KeyPressedEvent* a_event);


private:


};


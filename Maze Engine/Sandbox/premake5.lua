project "Sandbox"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "off"

	targetdir	("%{wks.location}/build/" .. dir .. "/%{prj.name}")
	objdir		("%{wks.location}/build-temp/" .. dir .. "/%{prj.name}")
	

	files
	{
		"src/**.h",
		"src/**.cpp"

	}

	includedirs
	{
		"%{IncludeDir.Maze}",
		"%{IncludeDir.spdlog}",
		"%{IncludeDir.GLAD}",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.imgui}",
		"%{IncludeDir.glm}",
		"%{IncludeDir.stb}"
	}
	

	links
	{
		"Maze Engine",
		"imgui"
	}



	filter "system:windows"
		systemversion "latest"

	filter "configurations:Debug"
		defines "MZ_DEBUG"
		symbols "On"
		runtime "Debug"
		
	filter "configurations:Release"
		defines "MZ_RELEASE"
		optimize "On"
		runtime "Release"

	filter "configurations:Dist"
		defines "MZ_Dist"
		optimize "On"
		runtime "Release"
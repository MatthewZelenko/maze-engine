project "Maze Engine"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "off"

	targetdir	("%{wks.location}/build/" .. dir .. "/%{prj.name}")
	objdir		("%{wks.location}/build-temp/" .. dir .. "/%{prj.name}")

	pchheader "mzpch.h"
	pchsource "src/mzpch.cpp"

	files
	{
		"src/**.h",
		"src/**.cpp",
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS",
		"GLFW_INCLUDE_NONE"
	}

	includedirs 
	{
		"src",
		"%{IncludeDir.spdlog}",
		"%{IncludeDir.GLAD}",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.imgui}",
		"%{IncludeDir.glm}",
		"%{IncludeDir.stb}"
	}

	links
	{
		"glad",
		"%{LibDir.GLFW}",
		"imgui",
		"opengl32"
	}



	filter "files:vendor/glad/**.c"
		flags { "NoPCH" }

	filter "system:windows"
		systemversion "latest"

	filter "configurations:Debug"
		defines "MZ_DEBUG"
		symbols "On"
		runtime "Debug"
		
	filter "configurations:Release"
		defines "MZ_RELEASE"
		optimize "On"
		runtime "Release"

	filter "configurations:Dist"
		defines "MZ_Dist"
		optimize "On"
		runtime "Release"
#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>


namespace Maze
{
	class Window
	{
	public:
		Window(uint32_t a_width, uint32_t a_height, const std::string& a_title);
		virtual ~Window();

		bool Init();
		void Destroy();

		void Update();
		void SwapBuffer();

		GLFWwindow* GetWindow() { return m_window; }
		inline const std::string& GetTitle() const { return m_title; }
		inline const int32_t GetWidth() const { return m_width; }
		inline const int32_t GetHeight() const { return m_height; }




	private:
		GLFWwindow* m_window;
		uint32_t m_width;
		uint32_t m_height;
		std::string m_title;
	};

}
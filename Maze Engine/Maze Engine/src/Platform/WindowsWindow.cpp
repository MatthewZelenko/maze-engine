#include "mzpch.h"
#include "WindowsWindow.h"
#include "Core/Log.h"
#include "Events/EventHandler.h"
#include "Events/ApplicationEvent.h"
#include "Events/KeyEvent.h"
#include "Events/MouseEvent.h"

static void GLFWErrorCallback(int a_error, const char* a_description)
{
	LOG_CORE_ERROR("GLFW: ({0}): {1}", a_error, a_description);
}

namespace Maze
{

	Window::Window(uint32_t a_width, uint32_t a_height, const std::string& a_title) : m_window(nullptr), m_width(a_width), m_height(a_height), m_title(a_title)
	{
	}

	Window::~Window()
	{
		Destroy();
	}

	bool Window::Init()
	{
		glfwSetErrorCallback(GLFWErrorCallback);

		if (!glfwInit())
			return false;


		m_window = glfwCreateWindow(m_width, m_height, m_title.c_str(), nullptr, nullptr);
		glfwMakeContextCurrent(m_window);



		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_SAMPLES, 4);



		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			LOG_CORE_ERROR("Failed to initialize Glad!");
			return false;
		}


		LOG_CORE_DEBUG("OpenGL Info:");
		LOG_CORE_DEBUG("    -Vendor: {0}", glGetString(GL_VENDOR));
		LOG_CORE_DEBUG("    -Renderer: {0}", glGetString(GL_RENDERER));
		LOG_CORE_DEBUG("    -Version: {0}", glGetString(GL_VERSION));


		glClearColor(0.05f, 0.075f, 0.125f, 1.0f);

		//VSYNC enabled
		glfwSwapInterval(1);



		glfwSetWindowCloseCallback(m_window, [](GLFWwindow* a_window)
			{
				EventHandler::Get().PushEvent(WindowCloseEvent());
			});

		glfwSetWindowSizeCallback(m_window, [](GLFWwindow* a_window, int a_width, int a_height)
			{
				EventHandler::Get().PushEvent(WindowResizeEvent(a_width, a_height));
			});

		glfwSetKeyCallback(m_window, [](GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
			{
				switch (a_action)
				{
				case GLFW_PRESS:
				{
					EventHandler::Get().PushEvent(KeyPressedEvent(a_key));
					break;
				}
				case GLFW_RELEASE:
				{
					EventHandler::Get().PushEvent(KeyReleasedEvent(a_key));
					break;
				}
				case GLFW_REPEAT:
				{
					EventHandler::Get().PushEvent(KeyPressedEvent(a_key, 1));
					break;
				}
				default:
					break;
				}
			});


		glfwSetCharCallback(m_window, [](GLFWwindow* a_window, unsigned int a_key)
			{
				EventHandler::Get().PushEvent(KeyTypedEvent(a_key));
			});

		glfwSetMouseButtonCallback(m_window, [](GLFWwindow* a_window, int a_button, int a_action, int a_mods)
			{
				switch (a_action)
				{
				case GLFW_PRESS:
				{
					EventHandler::Get().PushEvent(MouseButtonPressedEvent(a_button));
					break;
				}
				case GLFW_RELEASE:
				{
					EventHandler::Get().PushEvent(MouseButtonReleasedEvent(a_button));
					break;
				}
				default:
					break;
				}
			});
		glfwSetCursorPosCallback(m_window, [](GLFWwindow* a_window, double a_xPos, double a_yPos)
			{
				static double lastX, lastY;
				EventHandler::Get().PushEvent(MouseMovedEvent(a_xPos, a_yPos, a_xPos - lastX, a_yPos - lastY));
				lastX = a_xPos;
				lastY = a_yPos;

			});
		glfwSetScrollCallback(m_window, [](GLFWwindow* a_window, double a_xPos, double a_yPos)
			{
				EventHandler::Get().PushEvent(MouseScrolledEvent(a_xPos, a_yPos));

			});
		glfwSetCursorEnterCallback(m_window, [](GLFWwindow* a_window, int a_entered)
			{
				EventHandler::Get().PushEvent(MouseEnteredEvent(a_entered));
			});

		/*
		glfwSetMouseButtonCallback(m_window, [](GLFWwindow* a_window, int a_button, int a_action, int a_mods)
			{

				switch (a_action)
				{
				case GLFW_PRESS:
				{
					EventHandler::Get().PushEvent(MousePressedEvent(a_button));
					break;
				}
				case GLFW_RELEASE:
				{
					EventHandler::Get().PushEvent(MouseReleasedEvent(a_button));
					break;
				}
				}
			}

		glfwSetScrollCallback(m_window, [](GLFWwindow* a_window, double a_x, double a_y)
			{
				EventHandler::Get().PushEvent(MouseScrolledEvent(a_x, a_y));
			});

		glfwSetCursorPosCallback(m_window, [](GLFWwindow* a_window, double a_x, double a_y)
			{
				EventHandler::Get().PushEvent(MouseMovedEvent(a_x, a_y));
			});
			*/
		return true;
	}

	void Window::Destroy()
	{
		if (m_window)
		{
			glfwDestroyWindow(m_window);
			m_window = nullptr;
		}
		glfwTerminate();
	}

	void Window::Update()
	{
		glfwPollEvents();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void Window::SwapBuffer()
	{

		glfwSwapBuffers(m_window);
	}

}
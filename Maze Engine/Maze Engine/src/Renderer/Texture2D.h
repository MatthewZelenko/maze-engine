#pragma once
#include "Core/Core.h"

namespace Maze
{
	//An array of texture ids in relation to bound acive slots.
	static uint32_t s_boundTextures[32] = {0};


	class Texture2D
	{
	public:
		Texture2D();
		~Texture2D();

		void Bind(int a_index = 0);
		void UnBind();

		static Shared<Texture2D> CreateTextureFromFile(const std::string& a_filename);
		static Shared<Texture2D> CreateTextureFromData(const std::string& a_name, const char* a_data, int a_channel);

	private:
		std::string m_name;
		uint32_t m_textureID;
		int32_t m_width, m_height;
		int32_t m_channels;
		int32_t m_activeTexture;
	};
}
#pragma once
#include "Texture2D.h"
#include <glm/glm.hpp>

namespace Maze
{

	class Renderer2D
	{
	public:
		Renderer2D();
		~Renderer2D();

		void Render2D(Shared<Texture2D> a_texture, const glm::vec3& a_pos, const glm::vec2& a_size, glm::vec3 a_rotation = { 0,0,0 });
		void Render2D(Shared<Texture2D> a_texture, const glm::mat4& a_transform);

	private:



	};

}
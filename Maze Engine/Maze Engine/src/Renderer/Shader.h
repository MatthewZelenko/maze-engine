#pragma once
#include "Core/Core.h"

namespace Maze
{
	static uint32_t s_boundShader = 0;

	class Shader
	{
	public:
		Shader();
		~Shader();

		static Shared<Shader> CreateShaderFromFile(const std::string& a_filename);


	private:
		uint32_t m_shaderID;

	};

}
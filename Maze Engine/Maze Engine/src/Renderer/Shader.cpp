#include "mzpch.h"
#include "Shader.h"
#include <fstream>
#include <sstream>
#include "Core/Log.h"

namespace Maze
{

	Shader::Shader() : m_shaderID(0)
	{
	}
	Shader::~Shader()
	{
	}

	Shared<Shader> Shader::CreateShaderFromFile(const std::string& a_filename)
	{
		std::string shaderCode;
		{
			std::ifstream shaderFile(a_filename);
			std::stringstream ss;
			ss << shaderFile.rdbuf();
			shaderFile.close();
			shaderCode = ss.str();
		}

		std::string shaderType, shader;
		int32_t shaderIDIndex = 0;
		while (shaderIDIndex < shaderCode.size() - 1)
		{
			shaderIDIndex = shaderCode.find_first_of('[', shaderIDIndex);
			shaderType = shaderCode.substr(shaderIDIndex + 1, shaderCode.find_first_of(']', shaderIDIndex));

			int32_t shaderCount = shaderCode.find_first_of('[', shaderIDIndex) - (shaderIDIndex + shaderType.size() + 2);
			if (shaderIDIndex == shaderCode.npos)
			{
				shaderIDIndex == shaderCode.size() - 1;
			}
			shader = shaderCode.substr();

			if (shaderType == "vertex")
			{

			}
			else if (shaderType == "fragment")
			{

			}
			else
			{
				LOG_CORE_WARN("Shader type has not been implemented yet: [0]", shaderType);
				continue;
			}

		}
		shaderCode = vertexCode.substr(vertexCode.find('[')));



		return Shared<Shader>();
	}

}
#include "mzpch.h"
#include "Texture2D.h"
#include "Core/Log.h"
#include <stb/stb_image.h>
#include <glad/glad.h>

namespace Maze
{

	Texture2D::Texture2D() : m_textureID(0), m_height(0), m_width(0), m_channels(0), m_activeTexture(0)
	{
	}
	Texture2D::~Texture2D()
	{
		glDeleteTextures(1, &m_textureID);
	}

	void Texture2D::Bind(int a_index)
	{
		if (s_boundTextures[a_index] == m_textureID)
			return;

		glActiveTexture(a_index);
		glBindTexture(GL_TEXTURE_2D, m_textureID);
		m_activeTexture = a_index;
		s_boundTextures[a_index] = m_textureID;
	}
	void Texture2D::UnBind()
	{
		if (s_boundTextures[m_activeTexture] != m_textureID)
		{
			m_activeTexture = 0;
			return;
		}

		glActiveTexture(m_activeTexture);
		glBindTexture(GL_TEXTURE_2D, 0);
		s_boundTextures[m_activeTexture] = 0;
		m_activeTexture = 0;
	}

	Shared<Texture2D> Texture2D::CreateTextureFromFile(const std::string& a_filename)
	{
		Shared<Texture2D> tex(new Texture2D());

		tex->m_name = a_filename.substr(a_filename.find_last_of('/') + 1, a_filename.find_last_of('.'));


		glGenTextures(1, &tex->m_textureID);


		glActiveTexture(0);
		glBindTexture(GL_TEXTURE_2D, tex->m_textureID);


		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		stbi_uc* data = stbi_load(a_filename.c_str(), &tex->m_width, &tex->m_height, &tex->m_channels, 0);

		if (data)
		{
			int32_t ch = tex->m_channels == 3 ? GL_RGB : GL_RGBA;
			glTexImage2D(GL_TEXTURE_2D, 0, ch, tex->m_width, tex->m_height, 0, ch, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
			stbi_image_free(data);
		}
		else
			LOG_CORE_ERROR("png data is Null");


		if (s_boundTextures[0] != 0)
		{
			glActiveTexture(0);
			glBindTexture(GL_TEXTURE_2D, s_boundTextures[0]);
		}


		return tex;
	}
	Shared<Texture2D> Texture2D::CreateTextureFromData(const std::string& a_name, const char* a_data, int a_channel)
	{
		Shared<Texture2D> tex(new Texture2D());

		tex->m_name = a_name;


		glGenTextures(1, &tex->m_textureID);


		glActiveTexture(0);
		glBindTexture(GL_TEXTURE_2D, tex->m_textureID);


		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		glTexImage2D(GL_TEXTURE_2D, 0, a_channel, tex->m_width, tex->m_height, 0, a_channel, GL_UNSIGNED_BYTE, a_data);
		glGenerateMipmap(GL_TEXTURE_2D);


		if (s_boundTextures[0] != 0)
		{
			glActiveTexture(0);
			glBindTexture(GL_TEXTURE_2D, s_boundTextures[0]);
		}


		return tex;
	}

}
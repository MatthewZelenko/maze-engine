#pragma once
#include "Events/Event.h"
#include "Events/KeyEvent.h"
#include "Events/ApplicationEvent.h"
#include "Events/MouseEvent.h"
#include <unordered_map>
#include <functional>

#define SUBSCRIBE_LAYER_TO_EVENT(evnt, func) GetLayerStack().SubscribeToEvent<evnt>(this, std::bind(&func, this, std::placeholders::_1))


namespace Maze 
{
	class LayerStack;

	class Layer
	{
	public:
		Layer();
		virtual ~Layer();

		virtual void Update() = 0;
		virtual void Render() {}
		virtual void UIRender() {}

	
		LayerStack& GetLayerStack() { return *m_layerStack; }

	protected:

	private:
		friend class LayerStack;
		LayerStack* m_layerStack;
		//TODO::Does this Event* base work if the bound functions are Subclass types. Lambdas???
		std::unordered_map<uint32_t, std::function<bool(Event*)>> m_events;

	
	};

}
#include "mzpch.h"
#include "Time.h"

namespace Maze
{
	Time::Time() : m_deltaTime(0.0), m_runtime(0.0), m_frequency(1), m_lastTime(0), m_fps(0)
	{
	}

	Time::~Time()
	{
	}

	void Time::Init()
	{
		LARGE_INTEGER freq, lastTime;
		

		if(QueryPerformanceFrequency(&freq))
			m_frequency = freq.QuadPart;

		QueryPerformanceCounter(&lastTime);
		m_lastTime = lastTime.QuadPart;

		m_deltaTime = 0.0;
		m_runtime = 0.0;
		m_fps = 0.0;
	}

	void Time::Update()
	{
		LARGE_INTEGER currentTime;
		QueryPerformanceCounter(&currentTime);


		m_deltaTime = (double)(((currentTime.QuadPart - m_lastTime) * 1000000) / m_frequency);
		m_runtime += m_deltaTime;

		m_lastTime = currentTime.QuadPart;

		m_fps = 1.0 / m_deltaTime * 1000000.0;

	}

}
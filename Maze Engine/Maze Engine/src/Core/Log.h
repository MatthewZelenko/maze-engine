#pragma once
#pragma warning(push, 0)
#include <spdlog/spdlog.h>
#pragma warning(pop)


namespace Maze
{
	class Log
	{
	public:
		static void Init();

		static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_coreLogger; }
		static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_clientLogger; }

	private:
		static std::shared_ptr<spdlog::logger> s_coreLogger;
		static std::shared_ptr<spdlog::logger> s_clientLogger;
	};
}



#define LOG_CORE_TRACE(...) Maze::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define LOG_CORE_DEBUG(...) Maze::Log::GetCoreLogger()->debug(__VA_ARGS__)
#define LOG_CORE_INFO(...) Maze::Log::GetCoreLogger()->info(__VA_ARGS__)
#define LOG_CORE_WARN(...) Maze::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define LOG_CORE_ERROR(...) Maze::Log::GetCoreLogger()->error(__VA_ARGS__)
#define LOG_CORE_CRITICAL(...) Maze::Log::GetCoreLogger()->critical(__VA_ARGS__)


#define LOG_CLIENT_TRACE(...) Maze::Log::GetClientLogger()->trace(__VA_ARGS__)
#define LOG_CLIENT_DEBUG(...) Maze::Log::GetClientLogger()->debug(__VA_ARGS__)
#define LOG_CLIENT_INFO(...) Maze::Log::GetClientLogger()->info(__VA_ARGS__)
#define LOG_CLIENT_WARN(...) Maze::Log::GetClientLogger()->warn(__VA_ARGS__)
#define LOG_CLIENT_ERR(...) Maze::Log::GetClientLogger()->error(__VA_ARGS__)
#define LOG_CLIENT_CRITICAL(...) Maze::Log::GetClientLogger()->critical(__VA_ARGS__)

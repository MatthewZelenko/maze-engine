#include "mzpch.h"
#include "LayerStack.h"

namespace Maze
{
	LayerStack::LayerStack() : m_nextLayerIndex(0)
	{
	}
	LayerStack::~LayerStack()
	{
	}


	bool LayerStack::OnEvent(Event* a_event)
	{
		uint32_t eventID = a_event->GetEventType();

		//iterate layers from end to begin
		//If layer handles the event, break loop, stopping layers underneath from processing the event.
		bool handled = false;
		for (auto& layer = m_layers.rbegin(); layer != m_layers.rend(); ++layer)
		{
			auto iter = layer->get()->m_events.find(eventID);
			if (iter != layer->get()->m_events.end())
			{
				handled = true;
				if (iter->second(a_event))
					break;
			}
		}
		return handled;
	}

	void LayerStack::PopLayer(Shared<Layer> a_layer)
	{
		auto iter = std::find(Begin(), Begin() + m_nextLayerIndex - 1, a_layer);
		if (iter == m_layers.end())
			return;

		m_layers.erase(iter);
		m_nextLayerIndex--;
	}

	void LayerStack::PopOverlay(Shared<Layer> a_layer)
	{
		auto iter = std::find(Begin() + m_nextLayerIndex, End(), a_layer);
		if (iter == m_layers.end())
			return;

		m_layers.erase(iter);
	}
}
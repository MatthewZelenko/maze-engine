#pragma once

#define TIME_DELTA_IN_MICRO() Maze::Time::Get().GetDeltaTime()
#define TIME_DELTA_IN_MILLI() Maze::Time::Get().GetDeltaTime()		/ 1000.0
#define TIME_DELTA_IN_SECONDS() Maze::Time::Get().GetDeltaTime()	/ 1000000.0

#define TIME_FPS() Maze::Time::Get().GetFPS()

#define TIME_RUNTIME_IN_MICRO() Maze::Time::Get().GetRuntime()
#define TIME_RUNTIME_IN_MILLI() Maze::Time::Get().GetRuntime()		/ 1000.0
#define TIME_RUNTIME_IN_SECONDS() Maze::Time::Get().GetRuntime()	/ 1000000.0
#define TIME_RUNTIME_IN_MINUTES() Maze::Time::Get().GetRuntime()	/ 60000000.0
#define TIME_RUNTIME_IN_HOURS() Maze::Time::Get().GetRuntime()		/ 360000000.0


namespace Maze
{
	class Time
	{
	public:
		static Time& Get() { static Time s_time; return s_time; }

		void Init();
		void Update();

		//Milliseconds
		inline double const GetDeltaTime() const { return m_deltaTime; }

		inline double const GetRuntime() const { return m_runtime; }
		inline uint64_t const GetFPS() const { return m_fps; }

	private:
		Time();
		~Time();

		LONGLONG m_lastTime, m_frequency;
		//microseconds
		double m_runtime, m_deltaTime;

		uint64_t m_fps;

	};
}
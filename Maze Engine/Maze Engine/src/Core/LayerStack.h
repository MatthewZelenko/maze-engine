#pragma once
#include "Layer.h"
#include <vector>
#include "Core.h"
#include "Events/EventHandler.h"

#define LAYERSTACK_EVENT_LOOP(a_event, x) for (auto iter = m_layerStack->End(); iter != m_layerStack->Begin();)\
{\
	if ((*--iter)->x(a_event))\
	{\
		return true;\
	}\
}\

namespace Maze
{
	class LayerStack
	{
	public:
		LayerStack();
		~LayerStack();


		template <class T, class... Args>
		Shared<T> CreateLayer(Args... a_args);
		template <class T, class... Args>
		Shared<T> CreateOverlay(Args... a_args);

		template<class T>
		void SubscribeToEvent(Layer* a_layer, std::function<bool(T*)> a_func);

		bool OnEvent(Event* a_event);

		void PopLayer(Shared<Layer> a_layer);
		void PopOverlay(Shared<Layer> a_layer);

		std::vector<Shared<Layer>>::iterator Begin() { return m_layers.begin(); }
		std::vector<Shared<Layer>>::iterator End() { return m_layers.end(); }


	private:
		EventSubscriber m_eventSubscriber;
		std::vector<Shared<Layer>> m_layers;
		uint8_t m_nextLayerIndex;
	};

	template<class T, class ...Args>
	inline Shared<T> LayerStack::CreateLayer(Args ...a_args)
	{
		m_layers.emplace(m_layers.begin() + m_nextLayerIndex, std::make_shared<T>());

		Shared<T> layer = std::dynamic_pointer_cast<T>(m_layers[m_nextLayerIndex++]);

		layer->m_layerStack = this;
		layer->Init(a_args...);

		return  layer;
	}

	template<class T, class ...Args>
	inline Shared<T> LayerStack::CreateOverlay(Args ...a_args)
	{
		m_layers.emplace_back(std::make_shared<T>());
		
		Shared<T> layer = std::dynamic_pointer_cast<T>(m_layers.back());

		layer->m_layerStack = this;
		layer->Init(a_args...);

		return layer;
	}

	template<class T>
	inline void LayerStack::SubscribeToEvent(Layer* a_layer, std::function<bool(T*)> a_func)
	{
		uint32_t eventID = T::GetStaticType();

		auto iter = a_layer->m_events.find(eventID);
		assert((iter == a_layer->m_events.end()) && "Layer already has event subscribed!");

		a_layer->m_events[eventID] = [=](Event* a_event) 
		{ 
			return a_func(dynamic_cast<T*>(a_event));
		};

		//Allow layerstack to accept event
		m_eventSubscriber.SubscribeToEvent<T>(std::bind(&LayerStack::OnEvent, this, std::placeholders::_1));
	}


}
#pragma once

#include "Maze.h"



int main(int argc, char** argv)
{
	Maze::Log::Init();


	LOG_CORE_TRACE("Initializing Game...");
	//application = new();
	Maze::Application* app = Maze::Application::CreateApplication();

	if (!app)
	{
		LOG_CORE_TRACE("Terminating Game...");
		return -1;
	}

	LOG_CORE_TRACE("Running Game...");
	app->Run();

	LOG_CORE_TRACE("Terminating Game...");
	delete app;

	return 0;
}
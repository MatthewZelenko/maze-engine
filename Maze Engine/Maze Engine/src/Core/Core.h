#pragma once
#include <memory>

namespace Maze
{
	template<class T>
	using Shared = std::shared_ptr<T>;

	template<class T>
	using Unique = std::unique_ptr<T>;

	template<class T>
	using Weak = std::weak_ptr<T>;
}
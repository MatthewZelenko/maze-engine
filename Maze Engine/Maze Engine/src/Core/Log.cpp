#include "mzpch.h"
#include "Log.h"




#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>


namespace Maze
{

	std::shared_ptr<spdlog::logger> Log::s_coreLogger;
	std::shared_ptr<spdlog::logger> Log::s_clientLogger;


	void Log::Init()
	{


		std::vector<spdlog::sink_ptr> sinks;


		//trace
		sinks.emplace_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
		//debug
		sinks.emplace_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>("Maze Engine.log", true));

		//a sink is a place to log. such as console or file.
		sinks[0]->set_pattern("%^%T] || %n::%l::%$%v");
		
		
		std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> sinkPtr = std::dynamic_pointer_cast<spdlog::sinks::stdout_color_sink_mt>(sinks[0]);
		sinkPtr->set_color(spdlog::level::level_enum::trace, FOREGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_GREEN);
		sinkPtr->set_color(spdlog::level::level_enum::debug, FOREGROUND_INTENSITY | FOREGROUND_BLUE);
		sinkPtr->set_color(spdlog::level::level_enum::info, FOREGROUND_INTENSITY | FOREGROUND_GREEN);
		sinkPtr->set_color(spdlog::level::level_enum::warn, FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_RED);
		sinkPtr->set_color(spdlog::level::level_enum::err, FOREGROUND_INTENSITY | FOREGROUND_RED);
		sinkPtr->set_color(spdlog::level::level_enum::critical, FOREGROUND_BLUE | FOREGROUND_RED);

		sinks[1]->set_pattern("[%d/%m/%C : %T] || %n::%l::%v");

		//core
		s_coreLogger = std::make_shared<spdlog::logger>("Core", begin(sinks), end(sinks));
		spdlog::register_logger(s_coreLogger);
		s_coreLogger->set_level(spdlog::level::trace);
		s_coreLogger->flush_on(spdlog::level::trace);

		//client 
		s_clientLogger = std::make_shared<spdlog::logger>("Client", begin(sinks), end(sinks));
		spdlog::register_logger(s_clientLogger);
		s_clientLogger->set_level(spdlog::level::trace);
		s_clientLogger->flush_on(spdlog::level::trace);
	}

}
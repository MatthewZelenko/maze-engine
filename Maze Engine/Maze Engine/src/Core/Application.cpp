#include "mzpch.h"
#include "Application.h"
#include "Events/EventHandler.h"
#include "Events/ApplicationEvent.h"
#include "Core/Time.h"
#include <Platform/WindowsWindow.h>
#include <Core/LayerStack.h>
#include <Events/EventHandler.h>

namespace Maze
{
	Application* Application::s_instance = nullptr;


	Application::Application(const std::string& a_appName) : m_window(nullptr), m_isRunning(true), m_appName(a_appName), m_layerStack(new LayerStack()), m_eventSubscriber(new EventSubscriber())
	{
		s_instance = this;
	}
	Application::~Application()
	{
		s_instance = nullptr;
	}

	void Application::Run()
	{
		SystemInit();
		if (!Init())
			return;


		while (m_isRunning)
		{
			//Update System
			SystemUpdate();
			//Update application
			Update();
			//Update layers
			UpdateLayers();

			//Render System
			SystemRender();
			//Render application
			Render();
			//render layers
			RenderLayers();


			m_window->SwapBuffer();
		}

		Destroy();
		SystemDestroy();
	}
	void Application::SystemInit()
	{
		m_eventSubscriber->SubscribeToEvent<WindowCloseEvent>(std::bind(&Application::OnWindowClose, this, std::placeholders::_1));
		m_eventSubscriber->SubscribeToEvent<WindowResizeEvent>(std::bind(&Application::OnWindowResize, this, std::placeholders::_1));
		m_eventSubscriber->SubscribeToEvent<KeyPressedEvent>(std::bind(&Application::OnKeyPress, this, std::placeholders::_1));

		m_window = new Window(1280, 720, m_appName);
		m_window->Init();



		Time::Get().Init();


		m_uiLayer = GetLayerStack().CreateOverlay<UILayer>();
	}
	void Application::SystemUpdate()
	{
		//Update Time
		Time::Get().Update();

		//update window
		m_window->Update();
	}
	void Application::SystemRender()
	{

	}
	void Application::SystemDestroy()
	{
		m_uiLayer = nullptr;
		delete m_eventSubscriber; m_eventSubscriber = nullptr;		
		delete m_layerStack; m_layerStack = nullptr;
		delete m_window; m_window = nullptr;
		
	}

	void Application::UpdateLayers()
	{
		for (auto iter = m_layerStack->End(); iter != m_layerStack->Begin();)
		{
			(*--iter)->Update();
		}
	}
	void Application::RenderLayers()
	{
		for (auto iter = m_layerStack->End(); iter != m_layerStack->Begin();)
		{
			(*--iter)->Render();
		}

		m_uiLayer->Begin();
		for (auto iter = m_layerStack->End(); iter != m_layerStack->Begin();)
		{
			(*--iter)->UIRender();
		}
		m_uiLayer->End();
	}


	bool Application::OnWindowClose(WindowCloseEvent* a_event)
	{
		m_isRunning = false;
		LOG_CORE_CRITICAL("OnWindowClose");

		//handled
		return true;
	}
	bool Application::OnKeyPress(KeyPressedEvent* a_event)
	{
		if (a_event->GetKey() == KEY_ESCAPE)
		{
			m_isRunning = false;
			return true;
		}

		LOG_CORE_CRITICAL("OnKeyPress: {0}", a_event->GetKey());


		return false;
	}
	bool Application::OnWindowResize(WindowResizeEvent* a_event)
	{
		LOG_CORE_CRITICAL("OnWindowResize: {0}:{1}", a_event->GetWidth(), a_event->GetHeight());

		return false;
	}
	bool Application::OnKeyReleased(KeyReleasedEvent* a_event)
	{

		LOG_CORE_CRITICAL("OnKeyReleased: {0}", a_event->GetKey());

		return false;
	}
	bool Application::OnKeyTyped(KeyTypedEvent* a_event)
	{
		LOG_CORE_CRITICAL("OnKeyTyped: {0}", a_event->GetKey());

		return false;
	}
}
#pragma once
#include <string>
#include "LayerStack.h"
#include "Events/EventHandler.h"
#include "Platform/WindowsWindow.h"
#include "UI/UILayer.h"

namespace Maze
{

	class Application
	{
	public:
		virtual ~Application();

		static inline Application& Get() { return *s_instance; }
		static Application* CreateApplication();



		virtual bool Init() = 0;
		virtual void Update() = 0;
		virtual void Render() {}
		virtual void Destroy() = 0;

		void Run();



		inline void Close() { m_isRunning = false; }
		inline LayerStack& GetLayerStack() { return *m_layerStack; }
		inline Window& GetWindow() { return *m_window; }



	protected:
		Application(const std::string& a_appName);




	private:
		void SystemInit();
		void SystemUpdate();
		void SystemRender();
		void SystemDestroy();

		void UpdateLayers();
		void RenderLayers();

		bool OnWindowClose(WindowCloseEvent* e);
		bool OnKeyPress(KeyPressedEvent* e);
		bool OnWindowResize(WindowResizeEvent* e);
		bool OnKeyReleased(KeyReleasedEvent* a_event);
		bool OnKeyTyped(KeyTypedEvent* a_event);



		static Application* s_instance;
		bool m_isRunning;
		std::string m_appName;

		Window* m_window;
		EventSubscriber* m_eventSubscriber;
		LayerStack* m_layerStack;
		Shared<UILayer> m_uiLayer;
	};







	Application* CreateApplication();

}
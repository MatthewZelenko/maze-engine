#pragma once
#include <stdint.h>

#define STRINGIFY(x) #x

#define DEFINE_EVENT(type) static uint32_t GetStaticType() { return (uint32_t)(Maze::EventType::type); }\
								virtual uint32_t GetEventType() const override { return GetStaticType(); }\
								virtual const char* GetName() const override { return STRINGIFY(type##Event); }


namespace Maze
{


	enum class EventType : uint32_t
	{
		None = 0,
		WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
		KeyPressed, KeyReleased, KeyTyped,
		MouseEntered, MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled

	};
	class Event
	{
	public:
		Event();
		virtual ~Event();

		inline bool IsHandled() { return m_handled; }

		virtual uint32_t GetEventType() const = 0;
		virtual const char* GetName() const = 0;

	private:
		friend class EventHandler;
		bool m_handled;
	};

}
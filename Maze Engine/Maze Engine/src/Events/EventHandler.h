#pragma once
#include <map>

#include "Events/Event.h"
#include "Core/Log.h"


namespace Maze
{


	class EventHandler;
 


	class EventSubscriber
	{
	public:
		EventSubscriber();
		~EventSubscriber();


		template<class T>
		void SubscribeToEvent(std::function<bool(T*)> a_cb);

		template<class T>
		void UnsubscribeToEvent();
		void UnsubscribeToAll();


	private:
		friend class EventHandler;

		std::map<uint32_t/*event*/, uint32_t/*id*/> m_eventIDs;


	};


	template<class T>
	inline void EventSubscriber::SubscribeToEvent(std::function<bool(T*)> a_cb)
	{
		uint32_t type = T::GetStaticType();
		m_eventIDs[type] = EventHandler::Get().SubscribeToEvent<T>(a_cb);
	}

	template<class T>
	inline void EventSubscriber::UnsubscribeToEvent()
	{
		uint32_t type = T::GetStaticType();
		EventHandler::Get().UnsubscribeEvent(type, m_eventIDs[type]);
		m_eventIDs.erase(type);
	}





	class EventCallbackBase
	{
	public:
		EventCallbackBase();
		virtual ~EventCallbackBase() = default;

		bool Execute(Event* a_event) { return call(a_event); }

		uint32_t m_id;

	protected:
		virtual bool call(Event* a_event) = 0;
	};

	template<class EventT>
	class EventCallback : public EventCallbackBase
	{
	public:
		EventCallback(std::function<bool(EventT*)> a_cb) : m_cb(a_cb) {}
		~EventCallback() {}

		virtual bool call(Event* a_event) override { return m_cb(static_cast<EventT*>(a_event)); }

	private:

		std::function<bool(EventT*)> m_cb;
	};





	class EventHandler
	{
	public:

		static EventHandler& Get() { return s_instance; }

		EventHandler();
		~EventHandler();

		void Destroy();

		//returns id of callback
		template<class T>
		uint32_t SubscribeToEvent(std::function<bool(T*)> a_cb);

		void UnsubscribeEvent(uint32_t a_event, uint32_t a_id);

		template<class T>
		void PushEvent(T a_event);

	private:
		std::map<uint32_t/*Event ID*/, std::vector<EventCallbackBase*>> m_eventCallbacks;
		static uint32_t s_nextID;

		static EventHandler s_instance;
	};


	template<class T>
	inline uint32_t EventHandler::SubscribeToEvent(std::function<bool(T*)> a_cb)
	{
		uint32_t type = T::GetStaticType();
		m_eventCallbacks[type].emplace_back(new EventCallback<T>(a_cb));
		m_eventCallbacks[type].back()->m_id = s_nextID++;

		return (s_nextID - 1);
	}



	template<class T>
	inline void EventHandler::PushEvent(T a_event)
	{
		uint32_t type = T::GetStaticType();
		if (m_eventCallbacks.find(type) == m_eventCallbacks.end())
			return;

		//reset handled
		a_event.m_handled = false;

		size_t size = m_eventCallbacks[type].size();
		for (size_t i = 0; i < size; i++)
		{
			a_event.m_handled |= m_eventCallbacks[type][i]->Execute(&a_event);
		}
	}

}
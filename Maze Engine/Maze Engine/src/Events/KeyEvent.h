#pragma once
#include "Event.h"
#include "Core/KeyCodes.h"

namespace Maze
{

	class KeyEvent : public Event
	{
	public:
		virtual ~KeyEvent() = default;

		inline KeyCode GetKey() const { return m_key; }

	protected:
		KeyEvent(KeyCode a_key) : m_key(a_key) {}

		KeyCode m_key;
	};


	class KeyPressedEvent : public KeyEvent
	{
	public:
		KeyPressedEvent(KeyCode a_key, const uint16_t a_repeatCount = 0) : KeyEvent(a_key), m_repeatCount(a_repeatCount) {}
		~KeyPressedEvent() = default;

		inline uint16_t GetRepeatCount() const { return m_repeatCount; }

		DEFINE_EVENT(KeyPressed);


	private:
		uint16_t m_repeatCount;
	};



	class KeyReleasedEvent : public KeyEvent
	{
	public:
		KeyReleasedEvent(KeyCode a_key) : KeyEvent(a_key) {}
		~KeyReleasedEvent() = default;


		DEFINE_EVENT(KeyReleased);


	private:
	};


	class KeyTypedEvent : public KeyEvent
	{
	public:
		KeyTypedEvent(KeyCode a_key) : KeyEvent(a_key) {}
		~KeyTypedEvent() = default;


		DEFINE_EVENT(KeyTyped);


	private:
	};

}
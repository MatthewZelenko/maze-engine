#include "mzpch.h"
#include "ApplicationEvent.h"


namespace Maze
{


	WindowResizeEvent::WindowResizeEvent(uint32_t a_width, uint32_t a_height) : m_width(a_width), m_height(a_height)
	{
	}

	WindowResizeEvent::~WindowResizeEvent()
	{
	}

	WindowCloseEvent::WindowCloseEvent()
	{
	}

	WindowCloseEvent::~WindowCloseEvent()
	{
	}

}
#pragma once
#include "Event.h"


namespace Maze
{

	class WindowCloseEvent : public Event
	{
	public:
		WindowCloseEvent();
		~WindowCloseEvent();

		DEFINE_EVENT(WindowClose);
	};




	class WindowResizeEvent : public Event
	{
	public:
		WindowResizeEvent(uint32_t a_width, uint32_t a_height);
		~WindowResizeEvent();

		uint32_t GetWidth() { return m_width; }
		uint32_t GetHeight() { return m_height; }

		DEFINE_EVENT(WindowResize);

	private:
		uint32_t m_width, m_height;
	};

}
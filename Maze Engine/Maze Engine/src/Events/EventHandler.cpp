#include "mzpch.h"
#include "EventHandler.h"


namespace Maze
{


	EventHandler EventHandler::s_instance;
	uint32_t EventHandler::s_nextID = 1;

	EventHandler::EventHandler()
	{
	}

	EventHandler::~EventHandler()
	{
	}

	void EventHandler::Destroy()
	{
		auto iter = m_eventCallbacks.end();
		iter--;
		for (size_t i = m_eventCallbacks.size() - 1; i >= 0; i--)
		{
			for (size_t j = 0; j < iter->second.size(); j++)
			{
				delete iter->second[j];
			}
		}
		m_eventCallbacks.clear();
	}

	void EventHandler::UnsubscribeEvent(uint32_t a_event, uint32_t a_id)
	{
		if (m_eventCallbacks.find(a_event) == m_eventCallbacks.end())
			return;


		for (int32_t i = 0; i < m_eventCallbacks[a_event].size(); i++)
		{
			if (a_id == m_eventCallbacks[a_event][i]->m_id)
			{
				if (i != m_eventCallbacks[a_event].size() - 1)
				{
					m_eventCallbacks[a_event][i]->m_id = 0;
					m_eventCallbacks[a_event][i] = m_eventCallbacks[a_event][m_eventCallbacks[a_event].size() - 1];
				}
				m_eventCallbacks[a_event].pop_back();
				break;
			}
		}

		if (m_eventCallbacks[a_event].empty())
			m_eventCallbacks.erase(a_event);
	}




	EventSubscriber::EventSubscriber()
	{
	}

	EventSubscriber::~EventSubscriber()
	{
		UnsubscribeToAll();
	}

	void EventSubscriber::UnsubscribeToAll()
	{
		EventHandler eh = EventHandler::Get();
		auto iter = m_eventIDs.rbegin();
		while (iter != m_eventIDs.rend())
		{
			eh.UnsubscribeEvent(iter->first, iter->second);
			iter++;
		}
		m_eventIDs.clear();
	}

	EventCallbackBase::EventCallbackBase() : m_id(0)
	{

	}

}
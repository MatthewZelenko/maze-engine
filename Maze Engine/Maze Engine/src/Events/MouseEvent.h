#pragma once
#include "Event.h"
#include "Core/MouseCodes.h"


namespace Maze
{

	class MouseButtonEvent : public Maze::Event
	{
	public:
		virtual ~MouseButtonEvent() = default;

		inline MouseCode GetButton() const { return m_button; }

	protected:
		MouseButtonEvent(MouseCode a_button) : m_button(a_button) {}

		MouseCode m_button;
	};


	class MouseButtonPressedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonPressedEvent(MouseCode a_Mouse) : MouseButtonEvent(a_Mouse){}
		~MouseButtonPressedEvent() = default;


		DEFINE_EVENT(MouseButtonPressed);


	private:
	};



	class MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonReleasedEvent(MouseCode a_Mouse) : MouseButtonEvent(a_Mouse) {}
		~MouseButtonReleasedEvent() = default;


		DEFINE_EVENT(MouseButtonReleased);


	private:
	};



	class MouseMovedEvent : public Event
	{
	public:
		MouseMovedEvent(double  a_x, double  a_y, double  a_deltaX, double a_deltaY) : m_x(a_x), m_y(a_y), m_deltaX(a_deltaX), m_deltaY(a_deltaY) {}
		virtual ~MouseMovedEvent() = default;

		inline double GetX() const { return m_x; }
		inline double GetY() const { return m_y; }

		inline double GetXDelta() const { return m_deltaX; }
		inline double GetYDelta() const { return m_deltaY; }

		DEFINE_EVENT(MouseMoved);

	protected:

		double m_x, m_y;
		double m_deltaX, m_deltaY;
	};




	class MouseScrolledEvent : public Event
	{
	public:
		MouseScrolledEvent(double  a_x, double  a_y) : m_x(a_x), m_y(a_y) {}
		virtual ~MouseScrolledEvent() = default;

		inline double GetXDelta() const { return m_x; }
		inline double GetYDelta() const { return m_y; }

		DEFINE_EVENT(MouseScrolled);

	protected:

		double m_x, m_y;
	};

	class MouseEnteredEvent : public Event
	{
	public:
		MouseEnteredEvent(int  a_entered) : m_entered(a_entered) {}
		virtual ~MouseEnteredEvent() = default;

		inline double HasEntered() const { return m_entered; }

		DEFINE_EVENT(MouseEntered);

	protected:
		int m_entered;
	};

}

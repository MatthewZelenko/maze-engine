#pragma once
#include "Core\Layer.h"

namespace Maze
{
	class UILayer : public Layer
	{
	public:
		UILayer();
		~UILayer();


		void Init();
		void Update() override;
		void UIRender() override;
		void Begin();
		void End();

		bool OnMousePressed(Maze::MouseButtonPressedEvent* a_event);
		bool OnMouseReleased(Maze::MouseButtonReleasedEvent* a_event);
		bool OnMouseScrolled(Maze::MouseScrolledEvent* a_event);
		bool OnMouseMoved(Maze::MouseMovedEvent* a_event);
		bool OnMouseEntered(Maze::MouseEnteredEvent* a_event);
		bool OnKeyPressed(Maze::KeyPressedEvent* a_event);
		bool OnKeyReleased(Maze::KeyReleasedEvent* a_event);


		void BlockEvents(bool a_block) { m_blockEvents = a_block; }

	private:
		bool m_blockEvents = true;


	};
}
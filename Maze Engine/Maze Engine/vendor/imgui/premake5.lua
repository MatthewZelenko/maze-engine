project "imgui"
    kind "StaticLib"
    language "C"
    staticruntime "off"
    
	targetdir	("%{wks.location}/build/" .. dir .. "/%{prj.name}")
	objdir		("%{wks.location}/build-temp/" .. dir .. "/%{prj.name}")

    files
    {
        "include/**.h",
        "include/**.cpp",
    }

    includedirs
    {
        "include",
        "%{IncludeDir.GLFW}",
    }
    
    filter "system:windows"
        systemversion "latest"

    filter "configurations:Debug"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        runtime "Release"
        optimize "on"
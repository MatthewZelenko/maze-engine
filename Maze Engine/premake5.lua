include "Dependencies.lua"

workspace "Maze Engine"
	architecture "x86_64"
	startproject "Sandbox"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

dir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"


--Include all premake projects


group "Dependencies"
	include "Maze Engine/vendor/glad"
	include "Maze Engine/vendor/imgui"

group "Maze"
	include "Maze Engine"
	include "Sandbox"
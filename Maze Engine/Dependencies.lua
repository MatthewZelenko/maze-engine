IncludeDir = {}
IncludeDir["GLFW"] = "%{wks.location}/Maze Engine/vendor/GLFW/include"
IncludeDir["spdlog"] = "%{wks.location}/Maze Engine/vendor/spdlog/include"
IncludeDir["GLAD"] = "%{wks.location}/Maze Engine/vendor/glad/include"
IncludeDir["imgui"] = "%{wks.location}/Maze Engine/vendor/imgui/include"
IncludeDir["stb"] = "%{wks.location}/Maze Engine/vendor/stb/include"
IncludeDir["glm"] = "%{wks.location}/Maze Engine/vendor/glm/include"
IncludeDir["Maze"] = "%{wks.location}/Maze Engine/src/"

LibDir = {}
LibDir["GLFW"] = "%{wks.location}/Maze Engine/vendor/GLFW/bin/glfw3_mt.lib"
